package ru.tsc.avramenko.tm.controller;

import ru.tsc.avramenko.tm.api.controller.ICommandController;
import ru.tsc.avramenko.tm.api.service.ICommandService;
import ru.tsc.avramenko.tm.model.Command;
import ru.tsc.avramenko.tm.util.NumberUtil;

public class CommandController implements ICommandController {

    private final ICommandService commandService;

    public CommandController(ICommandService commandService) {
        this.commandService = commandService;
    }

    @Override
    public void showErrorCommand() {
        System.err.println("Error! Command not found!");
        System.exit(1);
    }

    @Override
    public void showErrorArgument() {
        System.err.println("Error! Argument not supported!");
        System.exit(1);
    }

    @Override
    public void exit() {
        System.exit(0);
    }

    @Override
    public void showInfo() {
        System.out.println("[INFO]");
        final int availableProcessors = Runtime.getRuntime().availableProcessors();
        final long freeMemory = Runtime.getRuntime().freeMemory();
        final long maxMemory = Runtime.getRuntime().maxMemory();
        final long totalMemory = Runtime.getRuntime().totalMemory();
        final long usedMemory = totalMemory - freeMemory;
        final String maxMemoryValue = NumberUtil.formatBytes(maxMemory);
        final String maxMemoryFormat = maxMemory == Long.MAX_VALUE ? "no limit" : maxMemoryValue;
        System.out.println("Available processors (cores): " + availableProcessors);
        System.out.println("Free memory: " + NumberUtil.formatBytes(freeMemory));
        System.out.println("Maximum memory: " + maxMemoryFormat);
        System.out.println("Total memory: " + NumberUtil.formatBytes(totalMemory));
        System.out.println("Used memory: " + NumberUtil.formatBytes(usedMemory));
    }

    @Override
    public void showAbout() {
        System.out.println("[ABOUT]");
        System.out.println("DEVELOPER: Avramenko Dmitry");
        System.out.println("E-MAIL: DimaAvramenko@yandex.ru");
    }

    @Override
    public void showVersion() {
        System.out.println("[VERSION]");
        System.out.println("1.0.0");
    }

    @Override
    public void showCommands() {
        System.out.println("[COMMANDS]");
        final Command[] commands = commandService.getTerminalCommands();
        for (final Command command : commands) showCommandValue(command.getName());
    }

    @Override
    public void showArguments() {
        System.out.println("[ARGUMENTS]");
        final Command[] commands = commandService.getTerminalCommands();
        for (final Command command : commands) showCommandValue(command.getArgument());
    }

    @Override
    public void showCommandValue(final String value) {
        if (value == null || value.isEmpty()) return;
        System.out.println(value);
    }

    @Override
    public void showHelp() {
        System.out.println("[HELP]");
        final Command[] commands = commandService.getTerminalCommands();
        int index = 0;
        for (final Command command : commands) {
            index++;
            System.out.println(index + ". " + command);
        }
    }

}
