package ru.tsc.avramenko.tm.api.service;

import ru.tsc.avramenko.tm.model.Command;

public interface ICommandService {
    Command[] getTerminalCommands();
}
